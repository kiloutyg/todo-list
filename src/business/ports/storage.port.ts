import * as Entities from 'src/domain/entities';
import { Result } from "../types";

export interface Storage {
    createTodolist: (todolist: Entities.TodoListInterface) => Result<Entities.TodoList>,
    readTodolist: (id: number) => Result<Entities.TodoList>,
    readAllTodolists: () => Result<Entities.TodoList[]>,
    updateTodolist: (todolist: Entities.TodoListInterface) => Result<Entities.TodoList>,
    deleteTodolist: (id: number) => Result<{}>,
}
