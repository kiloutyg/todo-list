import * as Entities from 'src/domain/entities'
import { Result } from '../types';

export interface Storage {
    createTodoList: (todolist: Entities.TodoListInterface) => Result<Entities.TodoList>,
    readTodoList: (id: number) => Result<Entities.TodoList>,
    readAllTodoLists: () => Result<Entities.TodoList[]>,
    updateTodoList: (todolist: Entities.TodoListInterface) => Result<Entities.TodoList>,
    deleteTodoList: (id: number) => Result<{}>,
}