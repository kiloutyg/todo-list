import * as Entities from "src/domain/entities";
import * as Ports from "../ports";
import { Result } from "../types";

export class AddTodolist {
    storagePort: Ports.Storage;
    loggerPort: Ports.Logger;

    constructor(storagePort: Ports.Storage, loggerPort: Ports.Logger) {
        this.storagePort = storagePort;
        this.loggerPort = loggerPort;
    }

    execute(todolist: Entities.TodoListInterface): Result<Entities.TodoList> {
        if (todolist.name.trim().length === 0) {
            this.loggerPort.error('Given name is empty');
            return {
                success: false,
                error: new Error('Given name is empty'),
            }
        }

        const result = this.storagePort.createTodolist(todolist);
        if (result.success) {
            this.loggerPort.debug(`Created id is ${result.value.id}`);
        }
        return result;
    }
}