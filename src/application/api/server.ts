import express, { Express, NextFunction, Request, Response } from 'express';

import * as Usecases from '../../business/usecases';
import * as Repositories from '../../infrastructure/repositories';

export class Server {
    app: Express;

    constructor() {
        this.app = express();
    }

    init(): void {
        this.app.use(express.urlencoded({ extended: true }));
        this.app.set('view engine', 'pug');

        this.app.use('/public', express.static('public'));

        this.app.post('/todolist', (request: Request, response: Response) => {
            // console.log()
            const { name, elements } = request.body;

            const storagePort = new Repositories.FileStorage('./resources/todolist.json');
            const loggerPort = new Repositories.LoggerRepository();
            const usecase = new Usecases.AddTodolist(storagePort, loggerPort);
            const result = usecase.execute({ name, elements });

            if (result.success) {
                response.json(result.value);
            } else {
                response.json({ error: result.error.message });
            }
        });
    }

    launch(port: number): void {
        this.app.listen(port, () => console.info(`Listening on port ${port}`));
    }
}

// app.get('/', (request: Request, response: Response) => response.redirect('/todolists'));

// app.get('/todolists', (request: Request, response: Response) => {
//     let todolists = Utils.getTodolists();
//     response.render('todo-lists', { todolists });
// });

// app.get('/todolist', (request: Request, response: Response) => {
//     response.render('todo-list-form', { todolist: undefined });
// });

// app.get('/todolist/:id', (request: Request, response: Response, next: NextFunction) => {
//     const id = parseInt(request.params.id, 10);

//     if (isNaN(id) || id <= 0) {
//         next(`Param id is not a positive number : ${id}`);
//     } else {
//         const todolist = Utils.getTodolist(id);
//         response.render('todo-list-form', { todolist });
//     }
// });

// app.post('/todolist/:id', (request: Request, response: Response, next: NextFunction) => {
//     const id = parseInt(request.params.id, 10);

//     if (isNaN(id) || id <= 0) {
//         next(`Param id is not a positive number : ${id}`);
//     } else {
//         const { name, elements } = request.body;
//         Utils.updateTodolist(new TodoList({ id, name, elements }));
//         response.redirect(`/todolist/${id}`);
//     }
// });
