import * as Entities from '../../domain/entities';
import { Result } from '../../business/types';
import * as Ports from '../../business/ports';
import fs from 'fs';
import path from 'path';

export class FileStorage implements Ports.Storage {
    filepath;

    constructor(filepath: string) {
        this.filepath = filepath;

    }

    createTodolist(todolist: Entities.TodoListInterface): Result<Entities.TodoList> {
        const tl = new Entities.TodoList(todolist)
        fs.writeFileSync(this.filepath, JSON.stringify([tl]));
        return {
            success: true,
            value: new Entities.TodoList(tl),
        }

        // return {
        //     success: false,
        //     error: new Error(),
        // }
    }

    readTodolist(id: number): Result<Entities.TodoList> {

        return {
            success: false,
            error: new Error(),
        }
    }

    readAllTodolists(): Result<Entities.TodoList[]> {
        return {
            success: false,
            error: new Error(),
        }
    }

    updateTodolist(todolist: Entities.TodoListInterface): Result<Entities.TodoList> {
        return {
            success: false,
            error: new Error(),
        }
    }

    deleteTodolist(id: number): Result<{}> {
        let readResult = this.readAllTodolists();
        if (readResult.success) {
            fs.writeFileSync(this.filepath, JSON.stringify(readResult.value.filter(t => t.id !== id)));
            return {
                success: true,
                value: {},
            }
        }
        return {
            success: false,
            error: new Error(),
        }
    }

    createDirIfNotExists(): void {
        const dir = path.dirname(this.filepath);
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
    }

}