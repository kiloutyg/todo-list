import * as Entities from 'src/domain/entities';
import { Result } from 'src/business/types';
import * as Ports from 'src/business/ports';

export class DatabaseStorage implements Ports.Storage {
    createTodolist(todolist: Entities.TodoListInterface): Result<Entities.TodoList> {
        return {
            success: false,
            error: new Error(),
        }
    }

    readTodolist(id: number): Result<Entities.TodoList> {
        return {
            success: false,
            error: new Error(),
        }
    }

    readAllTodolists(): Result<Entities.TodoList[]> {
        return {
            success: false,
            error: new Error(),
        }
    }

    updateTodolist(todolist: Entities.TodoListInterface): Result<Entities.TodoList> {
        return {
            success: false,
            error: new Error(),
        }
    }

    deleteTodolist(id: number): Result<{}> {
        return {
            success: false,
            error: new Error(),
        }
    }

}