import express from 'express';
import fs from 'fs';
import path from 'path';

class TodoList {
  id;
  name;
  elements;
  constructor(todolist) {
    this.id = todolist.id ?? 0;
    this.name = todolist.name;
    this.elements = todolist.elements ?? [];
  }
}

const filepath = "./resources/todolists.json";
function addTodolist(name, elements) {
  let todolists = getTodolists();
  const id = Math.max(...todolists.map((t) => t.id), 0) + 1;
  const todolist = new TodoList({ id, name, elements });
  todolists.push(todolist);
  fs.writeFileSync(filepath, JSON.stringify(todolists));
  return todolist;
}
function updateTodolist(todolist) {
  let todolists = getTodolists();
  const index = todolists.findIndex((t) => t.id === todolist.id);
  if (index > -1) {
    todolists[index] = todolist;
  }
  fs.writeFileSync(filepath, JSON.stringify(todolists));
}
function getTodolists() {
  const dir = path.dirname(filepath);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }
  if (fs.existsSync(filepath)) {
    let content = JSON.parse(fs.readFileSync(filepath, "utf-8"));
    return content.map((c) => new TodoList(c));
  } else {
    return [];
  }
}
function getTodolist(id) {
  return getTodolists().find((t) => t.id === id);
}

let app = express();
app.use(express.urlencoded({ extended: true }));
app.set("view engine", "pug");
app.use("/public", express.static("public"));
app.get("/", (request, response) => response.redirect("/todolists"));
app.get("/todolists", (request, response) => {
  let todolists = getTodolists();
  response.render("todo-lists", { todolists });
});
app.get("/todolist", (request, response) => {
  response.render("todo-list-form", { todolist: void 0 });
});
app.get("/todolist/:id", (request, response, next) => {
  const id = parseInt(request.params.id, 10);
  if (isNaN(id) || id <= 0) {
    next(`Param id is not a positive number : ${id}`);
  } else {
    const todolist = getTodolist(id);
    response.render("todo-list-form", { todolist });
  }
});
app.post("/todolist", (request, response) => {
  const { name, elements } = request.body;
  const todolist = addTodolist(name, elements);
  response.redirect(`/todolist/${todolist.id}`);
});
app.post("/todolist/:id", (request, response, next) => {
  const id = parseInt(request.params.id, 10);
  if (isNaN(id) || id <= 0) {
    next(`Param id is not a positive number : ${id}`);
  } else {
    const { name, elements } = request.body;
    updateTodolist(new TodoList({ id, name, elements }));
    response.redirect(`/todolist/${id}`);
  }
});
if (process.env.NODE_ENV === "production") {
  app.listen(5e3, () => console.log(`Listening on port 5000`));
}
const viteNodeApp = app;

export { viteNodeApp };
