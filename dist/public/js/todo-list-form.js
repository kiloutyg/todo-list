let addElementButton = document.getElementById('add-element');
if (addElementButton) {
    addElementButton.addEventListener('click', (event) => {
        let container = document.createElement('div');
        container.classList.add('element');

        let input = document.createElement('input');
        input.type = 'text';
        input.name = 'elements[]';

        let remove = document.createElement('button');
        remove.type = 'button';
        remove.innerText = '-';
        remove.addEventListener('click', (event) => event.target.parentNode.remove());

        container.append(input);
        container.append(remove);

        event.target.closest('.elements').append(container);
    });
}

document.querySelectorAll('.element > button').forEach(button => button.addEventListener('click', (event) => event.target.parentNode.remove()));